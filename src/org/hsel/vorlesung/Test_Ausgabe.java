package org.hsel.vorlesung;

import java.util.List;

public class Test_Ausgabe {
    public static void main(String[] args) throws TextFileFormatException {
        VorlesungsverzeichnisInterface vorlesungen = new Vorlesungsverzeichnis("vorlesung.txt");

        for (int i = 0; i < vorlesungen.getVorlesungsListeListe().size(); i++) {
            List<String> strings = vorlesungen.getVorlesungsListeListe().get(i);

            for (String s : strings) {
                System.out.print(s + " : ");
            }

            System.out.println();
        }
    }
}
