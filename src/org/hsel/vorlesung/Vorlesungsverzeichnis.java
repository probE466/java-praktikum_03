package org.hsel.vorlesung;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

/**
 * Verzeichnis von Vorlesungen.
 * <p>
 * Diese Klasse definiert ein Verzeichnis von Vorlesungen und
 * kann dieses sortieren und filtern.
 */
public class Vorlesungsverzeichnis implements VorlesungsverzeichnisInterface {

    /**
     * Liste der Vorlesungen.
     */
    private List<List<String>> vorlesungsListeListe = new ArrayList<>();

    /**
     * Index der Studierendengruppen in der Liste
     */
    private final int INDEX_GROUP = 0;

    /**
     * Index der Titel in der Liste
     */
    private final int INDEX_TITLE = 1;

    /**
     * Index der Professoren in der Liste
     */
    private final int INDEX_PROF = 2;

    /**
     * Kontstrukor für ein Vorlesungs Verzeichnis mit Pfadangabe.
     * <p>
     * Erzeugt die Klasse Vorlesungsverzeichnis indem aus der unter path angegebenen Textdatei gelsen wird.
     *
     * @param path Pfad zu der Textdatei
     * @throws TextFileFormatException Wenn Datei fehlerhaft formatiert, also 2 Semicolons teilen 3
     * Wörter, oder leer ist.
     */
    public Vorlesungsverzeichnis(String path) throws TextFileFormatException {
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(path))) {
            for (String line = bufferedReader.readLine(); line != null; line = bufferedReader.readLine()) {
                if (line.charAt(0) == ':')
                    throw new TextFileFormatException("Textfile ist nicht richtig formatiert");
                if (countColonsInLine(line) != 2)
                    throw new TextFileFormatException("Textfile ist ist nicht richtig formatiert");
                vorlesungsListeListe.add(Arrays.asList(line.split(":")));
            }
            if (vorlesungsListeListe.size() == 0)
                throw new TextFileFormatException("Textfile ist leer");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Zaehlt die Anzahl der Doppelpunkte in einer Zeile
     *
     * @param line Zu pruefende Zeile
     * @return Anzahl der Doppelpunkte
     */
    private int countColonsInLine(String line) {
        int colon = 0;
        for (char c : line.toCharArray()) if (c == ':') colon++;
        return colon;
    }

    public List<List<String>> getVorlesungsListeListe() {
        return vorlesungsListeListe;
    }

    @Override
    public List<String> titles() {
        List<String> titles = new ArrayList<>();
        String s;

        for (List<String> strings : vorlesungsListeListe) {
            s = strings.get(INDEX_TITLE);
            if (!titles.contains(s)) titles.add(s);
        }

        Collections.sort(titles);
        return titles;
    }

    @Override
    public Set<String> workaholics() {
        Map<String, Integer> profMap = new HashMap<>();
        Set<String> workaholics = new HashSet<>();

        for (List<String> strings : vorlesungsListeListe) {
            String key = strings.get(INDEX_PROF);
            if (profMap.containsKey(key)) {
                profMap.replace(key, profMap.get(key) + 1);
                workaholics.add(key);
            } else {
                profMap.put(key, 1);
            }
        }
        return workaholics;
    }

    @Override
    public Map<String, List<String>> groupToTitles() {
        Map<String, List<String>> stringListMap = new HashMap<>();

        String vorlesung;
        String group;
        for (List<String> strings : vorlesungsListeListe) {
            group = strings.get(INDEX_GROUP);
            vorlesung = strings.get(INDEX_TITLE);

            if (!stringListMap.containsKey(group)) {
                stringListMap.put(group, new ArrayList<>());
            }
            stringListMap.get(group).add(vorlesung);

        }
        return stringListMap;
    }


    @Override
    public Map<String, List<String>> multipleTitles() {
        Map<String, List<String>> stringListMap = new HashMap<>();
        List<String> toBeRemoved = new ArrayList<>();
        String vorlesung;
        String prof;

        for (List<String> strings : vorlesungsListeListe) {
            prof = strings.get(INDEX_PROF);
            vorlesung = strings.get(INDEX_TITLE);

            if (!stringListMap.containsKey(vorlesung)) {
                stringListMap.put(vorlesung, new ArrayList<>());
                stringListMap.get(vorlesung).add(prof);
            }
            if (!stringListMap.get(vorlesung).contains(prof)) {
                stringListMap.get(vorlesung).add(prof);
            }
        }

        for (Map.Entry<String, List<String>> stringListEntry : stringListMap.entrySet()) {
            if (stringListEntry.getValue().size() < 2) {
                toBeRemoved.add(stringListEntry.getKey());
            }
        }
        for (String s : toBeRemoved) {
            stringListMap.remove(s);
        }
        return stringListMap;
    }
}
