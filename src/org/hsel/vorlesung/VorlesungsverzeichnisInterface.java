package org.hsel.vorlesung;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Verzeichnis von Vorlesungen.
 * <p>
 * Dieses Interface definiert ein Verzeichnis von Vorlesungen und
 * kann dieses sortieren und filtern.
 */
public interface VorlesungsverzeichnisInterface {
    List<List<String>> getVorlesungsListeListe();

    /**
     * Liefert eine alphabetisch sortierte Liste mit den Titeln aller Vorlesungen.
     *
     * @return Liste der Titel
     */
    List<String> titles();

    /**
     * Liefert die Menge derjenigen Dozenten, die zwei oder mehr Vorlesungen halten.
     *
     * @return Set von Dozenten die 2 oder mehr Vorlesungen haben
     */
    Set<String> workaholics();

    /**
     * Liefert eine Map, die Studiengruppen auf Listen von Vorlesungstiteln abbildet.
     * <p>
     *
     * @return Map von Gruppen auf Vorlesungen
     */
    Map<String, List<String>> groupToTitles();

    /**
     * Liefert eine Map, die Vorlesungen auf Listen von Dozenten, die diese Vorlesungen halten, abbildet.
     * <p>
     * Als Schlüssel werden in der Map nur Vorlesungen verwendet,
     * die von unterschiedlichen Dozenten gehalten werden.
     *
     * @return Map von Vorlesungen auf Dozenten
     */
    Map<String, List<String>> multipleTitles();
}

