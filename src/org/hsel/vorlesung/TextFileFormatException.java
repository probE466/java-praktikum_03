package org.hsel.vorlesung;

public class TextFileFormatException extends Throwable {

    public TextFileFormatException(String message) {
        super(message);
    }
}
