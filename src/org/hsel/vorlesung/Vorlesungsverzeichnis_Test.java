package org.hsel.vorlesung;

import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;

import static org.junit.Assert.assertEquals;

public class Vorlesungsverzeichnis_Test {
    String filename = "db_junit.txt";
    PrintWriter pw;

    @Before
    public void setUp() throws IOException {
        pw = new PrintWriter(filename);
    }

    @Test
    public void Titles() throws IOException, TextFileFormatException {
        List<String> al = new ArrayList<String>();

        al.add("Elektrodynamik");
        al.add("Quantenmechanik");
        al.add("Quantenphysik");
        al.add("Relativit�tstheorie");
        al.add("Theoretische Physik");
        al.add("Thermodynamik");

        pw.print(
                "A1:Relativit�tstheorie:Einstein\n" +
                        "B2:Quantenmechanik:Heisenberg\n" +
                        "C2:Quantenphysik:Planck\n" +
                        "T4:Thermodynamik:Kelvin\n" +
                        "C2:Theoretische Physik:Kelvin\n" +
                        "B2:Thermodynamik:Planck\n" +
                        "T4:Quantenphysik:Planck\n" +
                        "B2:Elektrodynamik:Kelvin");
        pw.close();

        Vorlesungsverzeichnis l = new Vorlesungsverzeichnis(filename);
//		System.out.println(al);
//		System.out.println(l.titles());
        assertEquals(al, l.titles());
    }

    @Test
    public void Workaholics() throws IOException, TextFileFormatException {
        Set<String> s = new HashSet<String>();

        s.add("Planck");
        s.add("Kelvin");

        pw.print(
                "A1:Relativit�tstheorie:Einstein\n" +
                        "B2:Quantenmechanik:Heisenberg\n" +
                        "C2:Quantenphysik:Planck\n" +
                        "T4:Thermodynamik:Kelvin\n" +
                        "C2:Theoretische Physik:Kelvin\n" +
                        "B2:Thermodynamik:Planck\n" +
                        "T4:Quantenphysik:Planck\n" +
                        "B2:Elektrodynamik:Kelvin");
        pw.close();

        Vorlesungsverzeichnis l = new Vorlesungsverzeichnis(filename);
//		System.out.println(s);
//		System.out.println(l.workaholics());
        assertEquals(s, l.workaholics());
    }

    @Test
    public void GroupToTitles() throws IOException, TextFileFormatException {

        Map<String, List<String>> map2 = new HashMap<String, List<String>>();
        List<String> s1 = new ArrayList<String>();
        List<String> s2 = new ArrayList<String>();
        List<String> s3 = new ArrayList<String>();
        List<String> s4 = new ArrayList<String>();

        s1.add("Relativit�tstheorie");
        map2.put("A1", s1);
        s2.add("Quantenmechanik");
        s2.add("Thermodynamik");
        s2.add("Elektrodynamik");
        map2.put("B2", s2);
        s3.add("Quantenphysik");
        s3.add("Theoretische Physik");
        map2.put("C2", s3);
        s4.add("Thermodynamik");
        s4.add("Quantenphysik");
        map2.put("T4", s4);

        pw.print(
                "A1:Relativit�tstheorie:Einstein\n" +
                        "B2:Quantenmechanik:Heisenberg\n" +
                        "C2:Quantenphysik:Planck\n" +
                        "T4:Thermodynamik:Kelvin\n" +
                        "C2:Theoretische Physik:Kelvin\n" +
                        "B2:Thermodynamik:Planck\n" +
                        "T4:Quantenphysik:Planck\n" +
                        "B2:Elektrodynamik:Kelvin");
        pw.close();

        Vorlesungsverzeichnis l = new Vorlesungsverzeichnis(filename);
//		System.out.println(map2);
//		System.out.println(l.groupToTitles());
        assertEquals(map2, l.groupToTitles());
    }

    @Test
    public void multipleTitles() throws IOException, TextFileFormatException {

        Map<String, List<String>> map2 = new HashMap<String, List<String>>();
        List<String> s1 = new ArrayList<String>();

        s1.add("Kelvin");
        s1.add("Planck");
        map2.put("Thermodynamik", s1);

        pw.print(
                "A1:Relativit�tstheorie:Einstein\n" +
                        "B2:Quantenmechanik:Heisenberg\n" +
                        "C2:Quantenphysik:Planck\n" +
                        "T4:Thermodynamik:Kelvin\n" +
                        "C2:Theoretische Physik:Kelvin\n" +
                        "B2:Thermodynamik:Planck\n" +
                        "T4:Quantenphysik:Planck\n" +
                        "B2:Elektrodynamik:Kelvin");
        pw.close();

        Vorlesungsverzeichnis l = new Vorlesungsverzeichnis(filename);
//		System.out.println(map2);
//		System.out.println(l.multipleTitles());
        assertEquals(map2, l.multipleTitles());
    }

    @Test(expected = TextFileFormatException.class)
    public void Liste_zu_kurz() throws IOException, TextFileFormatException {
        pw.print(
                "A1:Relativit�tstheorie:Einstein\n" +
                        "B2:Quantenmechanik\n" +
                        "C2:Quantenphysik:Planck\n" +
                        "B2:Elektrodynamik:Kelvin");
        pw.close();
        Vorlesungsverzeichnis k = new Vorlesungsverzeichnis(filename);
    }

    @Test(expected = TextFileFormatException.class)
    public void Liste_zu_lang() throws IOException, TextFileFormatException {
        pw.print(
                "A1:Relativit�tstheorie:Einstein:Planck\n" +
                        "B2:Quantenmechanik:Heisenberg\n" +
                        "C2:Quantenphysik:Planck\n" +
                        "B2:Elektrodynamik:Kelvin");
        pw.close();
        Vorlesungsverzeichnis k = new Vorlesungsverzeichnis(filename);
    }

    @Test(expected = TextFileFormatException.class)
    public void Listenfeld_leer() throws IOException, TextFileFormatException {
        pw.print(
                "A1:Relativit�tstheorie:Einstein\n" +
                        "B2:Quantenmechanik:Heisenberg\n" +
                        ":Quantenphysik:Planck\n" +
                        "B2:Elektrodynamik:Kelvin");
        pw.close();
        Vorlesungsverzeichnis k = new Vorlesungsverzeichnis(filename);
    }
}
