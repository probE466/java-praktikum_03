package org.hsel.vorlesung;

import org.junit.Test;

import java.util.*;

import static org.junit.Assert.assertEquals;

public class VorlesungsverzeichnisTest {

    @Test
    public void it_can_initialize() throws TextFileFormatException {
        Vorlesungsverzeichnis v = new Vorlesungsverzeichnis("vorlesung.txt");
    }

    @Test(expected = TextFileFormatException.class)
    public void it_can_initialize_exception() throws TextFileFormatException {
        Vorlesungsverzeichnis v = new Vorlesungsverzeichnis("vorlesungFehler.txt");
    }

    @Test
    public void it_returns_titles() throws TextFileFormatException {
        Vorlesungsverzeichnis v = new Vorlesungsverzeichnis("vorlesung.txt");

        List<String> titlesTest = new ArrayList<>();
        titlesTest.add("Java 2");
        titlesTest.add("Algorithmen und Datenstrukturen");
        titlesTest.add("Audio-/Videotechnik");
        titlesTest.add("Mathematik 2");

        Collections.sort(titlesTest);

        assertEquals(titlesTest, v.titles());
    }

    @Test
    public void it_returns_workaholics() throws TextFileFormatException {
        Vorlesungsverzeichnis v = new Vorlesungsverzeichnis("vorlesung.txt");
        Set<String> workaholicsTest = new HashSet<>();

        workaholicsTest.add("von Coelln");

        assertEquals(workaholicsTest, v.workaholics());
    }

    @Test
    public void it_returns_groups() throws TextFileFormatException {
        Vorlesungsverzeichnis v = new Vorlesungsverzeichnis("vorlesung.txt");
        List<String> test = new ArrayList<>();

        test.add("Mathematik 2");
        test.add("Audio-/Videotechnik");

        assertEquals(test, v.groupToTitles().get("MT2"));
    }

    @Test
    public void it_returns_multiple_titles() throws TextFileFormatException {
        Vorlesungsverzeichnis v = new Vorlesungsverzeichnis("vorlesung.txt");
        List<String> test = new ArrayList<>();

        test.add("von Coelln");
        test.add("Rabe");

        assertEquals(1, v.multipleTitles().size());
        assertEquals(test, v.multipleTitles().get("Mathematik 2"));
    }
}